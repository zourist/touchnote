'use strict';

/**
 * @ngdoc overview
 * @name driverActivityApp
 * @description
 * # driverActivityApp
 *
 * Main module of the application.
 */
var driverActivityApp = angular.module('driverActivityApp',
  ['ngResource',
    'ngRoute',
    'mgcrea.ngStrap',
    'angularUtils.directives.dirPagination',
    'xml']);

var webApiBaseUrl = config_data.settings.apiBaseURL;
var rssFeedUrl = config_data.settings.rssFeedURL;

angular.forEach(config_data,function(key,value) {
  driverActivityApp.constant(key,value);
});

driverActivityApp.config( function ($routeProvider, $httpProvider) {
    $routeProvider
      .when('/', {
          templateUrl: 'views/main.html',
          controller: 'MainController'
      })
      .when('/detailsView/:driverId', {
        templateUrl: 'views/detailsView.html',
        controller: 'driverDetailsController'
      })
      .otherwise({
          redirectTo: '/'
      });

  $httpProvider.interceptors.push('xmlHttpInterceptor');
    /*
 * Ensure cookies created over xhr are available to JS.
 */
  $httpProvider.defaults.withCredentials = true;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
  $httpProvider.defaults.useXDomain = true;
});

