'use strict';

/**
 * @ngdoc function
 * @name driverActivityApp.controller:MainController
 * @description
 * # MainController
 * Controller of the driverActivityApp
 */
driverActivityApp.controller('MainController', function ($scope, driverActivityFactory, rssFeedFactory, $interval, $timeout) {
  /*Initialisation code (equivalent to "run" in an c# console application ) */
  /*Append all functions needed to be called on page initialisation & call
   this only at the end of controller to make sure all the function & variables had been initialised.*/
  /*it is also helpful to keep track of whats being invoked on controller load.*/
  $scope.init = function () {
    $scope.isFeedRefreshing = true;
    $scope.isActivityDataRefreshing = true;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.maxFeedLimit = 4;
    $scope.refresh();
    $scope.loadFeed();
  }

  $interval(function () {
    $scope.loadFeed();
  }, 15000);//10 seconds interval to demo quick refreshes.

  $scope.loadFeed = function () {
    $scope.stop();
    $scope.isFeedRefreshing = true;
    rssFeedFactory.parseFeed().then(function (response) {
      $timeout(function () {
        $scope.isFeedRefreshing = false;
        $scope.feeds = response.data;
        console.log("rss feed refreshed at " + new Date().toJSON());
        $scope.start();
      }, 2000);//2 seconds interval to show demo progress bar.
    })
  }

  $scope.pageChangeHandler = function (num) {
    console.log('Page changed to ' + num);
  };

  $scope.refresh = function () {
    //Load the driverActivity list
    driverActivityFactory.getDriverActivityList($scope.loadDriverActivityList);
  }

  $scope.loadDriverActivityList = function (response) {
    $scope.driverActivityList = response;
    $scope.isActivityDataRefreshing = false;
  };

  $scope.countdownValue = 0;
  function countdown() {
    $scope.countdownValue++;
    $scope.timeout = $timeout(countdown, 1000);
  }

  $scope.start = function() {
    countdown();
  };

  $scope.stop = function() {
    $timeout.cancel($scope.timeout);
    $scope.countdownValue = 0;
  };

  /*Invoke Page Initialisation function at the end of controller class to
   assure all variables/functions would have been loaded*/
  $scope.init();
});
