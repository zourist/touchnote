'use strict';

/**
 * @ngdoc function
 * @name driverActivityApp.controller:MainController
 * @description
 * # MainController
 * Controller of the driverActivityApp
 */
driverActivityApp.controller('driverDetailsController', function ($scope, driverActivityFactory, $routeParams) {
  /*Initialisation code (equivalent to "run" in an c# console application ) */
  /*Append all functions needed to be called on page initialisation & call
   this only at the end of controller to make sure all the function & variables had been initialised.*/
  /*it is also helpful to keep track of whats being invoked on controller load.*/
  $scope.init = function () {
    $scope.currentPage = 1;
    $scope.pageSize = 3;

    $scope.refresh();
  }

  $scope.pageChangeHandler = function (num) {
    console.log('Page changed to ' + num);
  };

  $scope.refresh = function () {
    //Load the driverActivity list
    console.log("Clicked driver id: " + $routeParams.driverId);
    driverActivityFactory.getDriverActivityDetails($routeParams.driverId, $scope.loadDriverActivityList);
  }

  $scope.loadDriverActivityList = function (response) {
    $scope.driverActivityList = response;

    var firstItem = $scope.driverActivityList[0];
    $scope.DriverSummary = "Driver Id: " + firstItem.driverId + " | Driver Name: "  + firstItem.firstname + " " + firstItem.surname;
  };

  /*Invoke Page Initialisation function at the end of controller class to
   assure all variables/functions would have been loaded*/
  $scope.init();
});
