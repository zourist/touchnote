﻿driverActivityApp.factory('driverActivityRestFactory', function ($resource, $http) {

  var driverActivityUrl = webApiBaseUrl + "driveractivityapi/:id";

  console.log(driverActivityUrl);

    /*This demonstrates use of rest api following resources approach*/
    return $resource(driverActivityUrl);

    //will return an object with following methods
    /*
    get()
    query()
    save()
    delete()
    */
});
