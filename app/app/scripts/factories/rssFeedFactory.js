
driverActivityApp.factory('rssFeedFactory', ['$http','$q', function ($http, $q) {

  var rssFeedUrl = webApiBaseUrl + "LogisticsManagerRssFeedApi?format=json";
  console.log(rssFeedUrl);
  return {
    parseFeed: function () {
      $http.defaults.useXDomain = true;
      return $http.get(rssFeedUrl);
    }
  }
}]);
