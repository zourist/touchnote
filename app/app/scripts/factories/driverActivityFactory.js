﻿driverActivityApp.factory('driverActivityFactory', ['driverActivityRestFactory', function (driverActivityRestFactory) {
    var driverActivityFactory =            {
                //Get all DriverActivity List
                getDriverActivityList: function (getDriverActivityListCallback) {
                    //make server call
                    driverActivityRestFactory.query(function (response) {
                        //invoke callback
                        if (getDriverActivityListCallback) {
                            getDriverActivityListCallback(response);
                        }
                        return response;
                    });
                },

                //Get DriverActivity Details
                getDriverActivityDetails: function (id, getDriverActivityDetailsCallback) {
                    //make server call
                    driverActivityRestFactory.get({ id: id }, function (response) {
                        //invoke callback
                        if (getDriverActivityDetailsCallback && response.isSuccessful) {
                          getDriverActivityDetailsCallback(response.driverDetails);
                        }
                    });
                }
            };
    return driverActivityFactory;
}]);
