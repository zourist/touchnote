﻿//Added for running tests in VS with resharper
/// <reference path="../../../bower_components/angular/angular.js" />
/// <reference path="../../../bower_components/angular-mocks/angular-mocks.js" />
/// <reference path="../../../bower_components/angular-resource/angular-resource.js" />
/// <reference path="../../../bower_components/angular-route/angular-route.js" />
/// <reference path="../../../bower_components/angular-strap/dist/angular-strap.js" />
/// <reference path="../../../bower_components/angular-strap/dist/angular-strap.tpl.js" />
/// <reference path="../../../app/scripts/app.js" />
/// <reference path="../../../app/scripts/controllers/main.js" />
/// <reference path="../../../app/scripts/factories/driverActivityRestFactory.js" />
/// <reference path="../../../app/scripts/factories/driverActivityFactorySpec.js" />
/// <reference path="../../../node_modules/jasmine-standalone-2.1.2/lib/jasmine-2.1.2/jasmine.js" />
/// <reference path="../../mock/services/mock.js" />

'use strict';

/* jasmine specs for controllers go here */
describe('driverActivityFactory tests', function () {
    var $httpBackend;
    // Load  module.
    beforeEach(angular.mock.module('driverActivityApp'));
    // Setup the mock service in an UserImplementation module.
    beforeEach(inject(function (_$httpBackend_) {
        $httpBackend = _$httpBackend_;

        //Get all DriverActivity List
        var response = [];
        $httpBackend.expectGET('/http:////localhost:12345//api//driverActivitylist?format=json//:id').respond(response.push(MockData.driverActivityList));

    }));

    it('Check for all required functions',
            inject(function (driverActivityFactory) {
                expect(angular.isFunction(driverActivityFactory.getDriverActivityList)).toBe(true);
                expect(angular.isFunction(driverActivityFactory.getDriverActivityDetails)).toBe(true);
            }));

    it('Test getDriverActivityList() function : returns json object',
            inject(function (driverActivityFactory) {

                driverActivityFactory.getDriverActivityList(function (response) {
                    expect(response).toEqual(MockData.driverActivityList);
                });
            }));

    it('Test getDriverActivityList() function : returns 3 items',
            inject(function (driverActivityFactory) {

                driverActivityFactory.getDriverActivityList(function (response) {
                    expect(response.length).toEqual(3);
                });
            }));

    it('should add the new DriverActivityItem using driverActivityFactory service',
        inject(function (driverActivityFactory) {
        var driverActivityItem = {
            "id": 4,
            "task": "Test4",
            "dueDate": "2015-01-01 00:00:00.000"
        }

        var mockResponse = {
            "isSuccessful": true,
            "responseCode": "Operation completed successfully"
        }

        $httpBackend.expectPOST('/http:////localhost:12345//api//driverActivitylist?format=json//:id', {}).respond([mockResponse]);

        driverActivityFactory.createDriverActivityItem(driverActivityItem, function (response) {
            expect(response.isSuccessful).toEqual(true);
            expect(response.responseCode).toEqual('Operation completed successfully');
        });
    }));
});
