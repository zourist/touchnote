@echo off

rem Ensure this Node.js and npm are first in the PATH
set PATH=%APPDATA%\npm;%~dp0;%PATH%

setlocal enabledelayedexpansion
pushd "%~dp0"

rem Figure out the node version.
set print_version=node.exe -p -e "process.versions.node + ' (' + process.arch + ')'"
for /F "usebackq delims=" %%v in (`%print_version%`) do set version=%%v

rem Print message.
if exist npm.cmd (
  echo Your environment has been set up for using Node.js !version! and npm.
) else (
  echo Your environment has been set up for using Node.js !version!.
)
call npm install -g karma
call npm install -g karma-jasmine@2_0
call npm install -g karma-junit-reporter
call npm install -g karma-html-reporter
call npm install -g karma-story-reporter
call npm install -g karma-phantomjs-launcher
call npm install -g karma-chrome-launcher
call npm install -g istanbul
call npm install -g karma-coverage
call npm install -g karma-cli

echo Karm setup is complete
pause

popd
endlocal
