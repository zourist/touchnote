﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using TechExercise.Models.DTOs;
using TechExercise.Extensions;

namespace TechExercise.Models
{
    public class DriverActivityService : IDriverActivityService
    {
        readonly string _xmlFilePath = string.Empty;

        public DriverActivityService()
        {
            string appDataDirectory = HttpContext.Current.ApplicationInstance.Server.MapPath("~/App_Data");
            string xmlFileName = System.Configuration.ConfigurationManager.AppSettings["XmlFilePath"];
            _xmlFilePath = Path.Combine(appDataDirectory, xmlFileName);
        }

        public List<DriverDataDto> GetDriversData()
        {
            var xmlDoc = XDocument.Load(_xmlFilePath);
            var result = xmlDoc.Descendants("Data")
                       .Descendants("DriverActivity")
                       .Select(toDoItem =>
                       {
                           var traceStartDate = DateParser(toDoItem.Element("Trace_Start").Value);
                           var traceEndDate = DateParser(toDoItem.Element("Trace_End").Value);
                           var activityStartDate = DateParser(toDoItem.Element("Activity_Start").Value);
                           var activityEndDate = DateParser(toDoItem.Element("Activity_End").Value);

                           return new DriverDataDto
                           {
                               DriverId = Convert.ToInt32(toDoItem.Element("Driver_ID").Value),
                               Surname = toDoItem.Element("Surname").Value,
                               Firstname = toDoItem.Element("Forename").Value,
                               Registration = toDoItem.Element("Registration").Value,
                               TraceKey = toDoItem.Element("Trace_Key").Value,
                               ActivityType = toDoItem.Element("Activity_Type").Value,
                               TraceStartDate = traceStartDate,
                               TraceEndDate = traceEndDate,
                               ActivityStartDate = activityStartDate,
                               ActivityEndDate = activityEndDate
                           };
                       }).DistinctBy(x => x.DriverId).OrderBy(a => a.Firstname).ToList();

            return result;
        }

        public List<DriverDataDto> GetDriverDataById(int id)
        {
            var xmlDoc = XDocument.Load(_xmlFilePath);
            return xmlDoc.Descendants("Data")
                       .Descendants("DriverActivity")
                       .Where(item => item.Element("Driver_ID").Value == id.ToString())
                       .Select(toDoItem =>
                       {
                           var traceStartDate = DateParser(toDoItem.Element("Trace_Start").Value);
                           var traceEndDate = DateParser(toDoItem.Element("Trace_End").Value);
                           var activityStartDate = DateParser(toDoItem.Element("Activity_Start").Value);
                           var activityEndDate = DateParser(toDoItem.Element("Activity_End").Value);

                           return new DriverDataDto
                           {
                               DriverId = Convert.ToInt32(toDoItem.Element("Driver_ID").Value),
                               Surname = toDoItem.Element("Surname").Value,
                               Firstname = toDoItem.Element("Forename").Value,
                               Registration = toDoItem.Element("Registration").Value,
                               TraceKey = toDoItem.Element("Trace_Key").Value,
                               ActivityType = toDoItem.Element("Activity_Type").Value,
                               TraceStartDate = traceStartDate,
                               TraceEndDate = traceEndDate,
                               ActivityStartDate = activityStartDate,
                               ActivityEndDate = activityEndDate
                           };
                       }).OrderByDescending(e => e.ActivityStartDate).ToList();

        }

        public bool DeleteDriverItem(int id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDriverData(DriverDataDto driverActivityDto)
        {
            throw new NotImplementedException();
        }

        public bool AddDriverData(DriverDataDto driverActivityDto)
        {
            throw new NotImplementedException();
        }

        #region Helper Methods
        public DateTime DateParser(string nullableDate)
        {
            try
            {
                var nullableDateValue = string.IsNullOrEmpty(nullableDate) ? DateTime.MinValue : Convert.ToDateTime(nullableDate);
                return nullableDateValue;
            }
            catch (Exception EX)
            {
                //Implement logging 
                return DateTime.MinValue;
            }
        }
        #endregion
    }
}