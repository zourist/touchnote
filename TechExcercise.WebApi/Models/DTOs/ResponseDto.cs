﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechExercise.Models.DTOs
{
    public class ResponseDto
    {
        public bool IsSuccessful { get; set; }
        public string ResponseCode { get; set; }
    }
}
