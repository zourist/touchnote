﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechExercise.Models.DTOs
{
    public class DriverResponseDto : ResponseDto
    {
        public List<DriverDataDto> DriverDetails { get; set; }
    }
}
