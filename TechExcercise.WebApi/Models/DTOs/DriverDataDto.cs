﻿using System;

namespace TechExercise.Models.DTOs
{
    public class DriverDataDto
    {
        public int DriverId { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string TraceKey { get; set; }
        public string Registration { get; set; }
        public string ActivityType { get; set; }
        public DateTime TraceStartDate { get; set; }
        public DateTime TraceEndDate { get; set; }
        public DateTime ActivityStartDate { get; set; }
        public DateTime ActivityEndDate { get; set; }
    }
}