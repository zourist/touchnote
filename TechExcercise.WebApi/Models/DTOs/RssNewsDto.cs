﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TechExercise.Models
{
   public class RssNewsDto
    {       
       public string Description { get; set; }
       public string Link { get; set; }
       public string Title { get; set; }
       public  DateTime PubDate { get; set; }
    }
}
