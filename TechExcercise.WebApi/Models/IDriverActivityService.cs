﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechExercise.Models.DTOs;

namespace TechExercise.Models
{
    interface IDriverActivityService
    {
        List<DriverDataDto> GetDriversData();
        List<DriverDataDto>  GetDriverDataById(int id);
        bool DeleteDriverItem(int id);
        bool UpdateDriverData(DriverDataDto driverActivityDto);
        bool AddDriverData(DriverDataDto driverActivityDto);
    }
}
