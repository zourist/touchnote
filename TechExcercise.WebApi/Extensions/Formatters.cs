﻿using System.Net.Http.Formatting;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TechExercise.Extensions
{
    public partial class AppConfig
    {
        public class Formatters
        {
            public static void Register(HttpConfiguration httpConfiguration)
            {
                var formatters = httpConfiguration.Formatters;
                var jsonFormatter = formatters.JsonFormatter;
                var settings = jsonFormatter.SerializerSettings;
                settings.Formatting = Formatting.Indented;
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

                httpConfiguration.Formatters.JsonFormatter.MediaTypeMappings.Add(new QueryStringMapping("format", "json", "application/json"));
                httpConfiguration.Formatters.XmlFormatter.MediaTypeMappings.Add(new QueryStringMapping("format", "xml", "application/xml"));
                httpConfiguration.Formatters.XmlFormatter.UseXmlSerializer = true;
            }
        }
    }


}