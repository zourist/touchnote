﻿using System;
using log4net;
using log4net.Config;
using TechExercise.Interfaces;

namespace TechExercise.Logging
{
    public class Log4NetLoggingProvider : ILoggingProvider
    {
        private readonly ILog logger;

        public Log4NetLoggingProvider(string name)
        {
            XmlConfigurator.Configure();
            this.logger = LogManager.GetLogger(name);
        }

        public Log4NetLoggingProvider(Type type)
        {
            XmlConfigurator.Configure();
            this.logger = LogManager.GetLogger(type);
        }

        public void Debug(string message)
        {
            this.logger.Debug(message);
        }

        public void Error(string message)
        {
            this.logger.Error(message);
        }

        public void Info(string message)
        {
            this.logger.Info(message);
        }

        public void Trace(string message)
        {
            this.logger.Info(message);
        }

        public void Warn(string message)
        {
            this.logger.Warn(message);
        }
    }
}