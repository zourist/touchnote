﻿using System;
using TechExercise.Interfaces;

namespace TechExercise.Logging
{
    public class LogWriter : ILogWriter
    {
        private readonly ILoggingProvider loggingProvider;

        public LogWriter(ILoggingProvider loggingProvider)
        {
            this.loggingProvider = loggingProvider;
        }

        public void Debug(string message)
        {
            this.loggingProvider.Debug(message);
        }

        public void Error(string message)
        {
            this.loggingProvider.Error(message);
        }

        public void Error(Exception exception)
        {
            this.loggingProvider.Error(exception.Message);
        }

        public void Info(string message)
        {
            this.loggingProvider.Info(message);
        }

        public void Trace(string message)
        {
            this.loggingProvider.Trace(message);
        }

        public void Warn(string message)
        {
            this.loggingProvider.Warn(message);
        }
    }
}