﻿using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using LightInject;
using TechExercise.Interfaces;
using TechExercise.Logging;

namespace TechExercise
{
    public class Bootstrapper
    {
        private IServiceContainer container;
        private ILogWriter logWriter;

        public void Run()
        {
            this.ConfigureContainer();
            this.LogStartup();

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void ConfigureContainer()
        {
            this.container = new ServiceContainer();

            this.RegisterSingletons();
            this.RegisterTransientTypes();

            this.container.RegisterControllers(typeof(MvcApplication).Assembly);
            this.container.EnableMvc();
        }

        private void LogStartup()
        {
            this.logWriter = this.container.GetInstance<ILogWriter>();
            this.logWriter.Info("Starting DriverDataDto Application");
        }

        private void RegisterSingletons()
        {
            var loggingProvider = new Log4NetLoggingProvider("DriverDataDto LogWriter");
            this.container.RegisterInstance(typeof(ILoggingProvider), loggingProvider);
            this.container.Register<ILogWriter, LogWriter>(new PerContainerLifetime());
        }

        private void RegisterTransientTypes() {}
    }
}