﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Net;

namespace TechExercise.Extensions
{

    public partial class AppConfig
    {
        /// <summary>
        /// Allow AJAX calls from other apps on other domains to access our service / API layer.  This is 
        /// achieved by adding Access-Control-  headers to our responses.  The browser will then determine 
        /// whether or not it should display the returned data.
        /// See:    http://blogs.msdn.com/b/carlosfigueira/archive/2012/02/20/implementing-cors-support-in-asp-net-web-apis.aspx
        ///         http://code.msdn.microsoft.com/Implementing-CORS-support-a677ab5d
        /// By: CarlosFigueira
        /// </summary>
        public class CorsHandler : DelegatingHandler
        {
            const string Origin = "Origin";
            const string AccessControlRequestMethod = "Access-Control-Request-Method";
            const string AccessControlRequestHeaders = "Access-Control-Request-Headers";
            const string AccessControlAllowOrigin = "Access-Control-Allow-Origin";
            const string AccessControlAllowMethods = "Access-Control-Allow-Methods";
            const string AccessControlAllowHeaders = "Access-Control-Allow-Headers";
            const string AccessControlAllowCredentials = "Access-Control-Allow-Credentials";
            const string AccessControlExposeHeaders = "Access-Control-Expose-Headers";

            protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                bool isCorsRequest = request.Headers.Contains(Origin);
                bool isPreflightRequest = (request.Method == HttpMethod.Options);

                if (isCorsRequest)
                {
                    if (isPreflightRequest)
                    {
                        return Task.Factory.StartNew<HttpResponseMessage>(() =>
                        {
                            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                            response.Headers.Add(AccessControlAllowOrigin, request.Headers.GetValues(Origin).First());


                            if (request.Headers.Contains(AccessControlRequestMethod))
                            {
                                string accessControlRequestMethod =
                                    request.Headers.GetValues(AccessControlRequestMethod).FirstOrDefault();
                                if (accessControlRequestMethod != null)
                                {
                                    response.Headers.Add(AccessControlAllowMethods, accessControlRequestMethod);
                                }
                            }


                            if (request.Headers.Contains(AccessControlRequestHeaders))
                            {
                                string requestedHeaders = string.Join(", ",
                                                                      request.Headers.GetValues(AccessControlRequestHeaders));
                                if (!string.IsNullOrEmpty(requestedHeaders))
                                {
                                    response.Headers.Add(AccessControlAllowHeaders, requestedHeaders);
                                }
                            }

                            response.Headers.Add(AccessControlAllowCredentials, "true");

                            return response;
                        }, cancellationToken);
                    }
                    else
                    {
                        return base.SendAsync(request, cancellationToken).ContinueWith<HttpResponseMessage>(t =>
                        {
                            HttpResponseMessage resp = t.Result;
                            resp.Headers.Add(AccessControlAllowOrigin, request.Headers.GetValues(Origin).First());

                            resp.Headers.Add(AccessControlAllowCredentials, "true");
                            return resp;
                        });
                    }
                }
                else
                {
                    return base.SendAsync(request, cancellationToken);
                }
            }
        }
    }

}