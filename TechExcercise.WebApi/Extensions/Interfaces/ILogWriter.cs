﻿using System;

namespace TechExercise.Interfaces
{
    public interface ILogWriter
    {
        void Debug(string message);
        void Error(string message);
        void Error(Exception exception);
        void Info(string message);
        void Trace(string message);
        void Warn(string message);
    }
}