﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechExercise.Interfaces
{
    public interface ILoggingProvider
    {
        void Debug(string message);
        void Error(string message);
        void Info(string message);
        void Trace(string message);
        void Warn(string message);
    }
}