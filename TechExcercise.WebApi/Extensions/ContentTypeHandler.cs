﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace TechExercise.Extensions
{

    public partial class AppConfig
    {
        /// <summary>
        /// Handlre to patch up missing content-type from sloppy browers
        /// We assume that http POSTS should be sending JSON
        /// JD: but only if a post with with content.
        /// </summary>
        public class ContentTypeHandler : DelegatingHandler
        {
            protected override Task<HttpResponseMessage> SendAsync(
                HttpRequestMessage request,
                CancellationToken cancellationToken)
            {
                if (request.Method == HttpMethod.Post
                   && request.Content != null && request.Content.Headers.ContentType == null)
                {
                    request.Content.Headers.ContentType
                        = new MediaTypeHeaderValue("application/json");
                }

                return base.SendAsync(request, cancellationToken);
            }
        }
    }

}