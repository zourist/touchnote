﻿using System.Collections.Generic;
using System.Web.Http;
using TechExercise.Models;
using TechExercise.Models.DTOs;

namespace TechExercise.Controllers
{
    public class DriverActivityApiController : ApiController
    {
        private DriverActivityService _driverActivityService;

        public IEnumerable<DriverDataDto> Get()
        {
            _driverActivityService = new DriverActivityService();
            var _driverActivityList = _driverActivityService.GetDriversData();
            return _driverActivityList;
        }

        public ResponseDto Get(int id)
        {
            _driverActivityService = new DriverActivityService();
            var _driverActivityList = _driverActivityService.GetDriverDataById(id);
            return new DriverResponseDto()
            {
                IsSuccessful = true,
                DriverDetails = _driverActivityList,
                ResponseCode = @"Operation completed successfully"
            };
        }

        [HttpPost]
        public ResponseDto Post(DriverDataDto driverActivityDto)
        {
            _driverActivityService = new DriverActivityService();
            if (_driverActivityService.AddDriverData(driverActivityDto))
            {
                return new ResponseDto()
                {
                    IsSuccessful = true,
                    ResponseCode = @"Operation completed successfully"
                };
            }
            return new ResponseDto()
            {
                IsSuccessful = false,
                ResponseCode = @"Something went wrong."
            };
        }


        [HttpPut]
        public ResponseDto Put(DriverDataDto driverActivityDto)
        {
            _driverActivityService = new DriverActivityService();
            if (_driverActivityService.UpdateDriverData(driverActivityDto))
            {
                return new ResponseDto(){
                    IsSuccessful = true,
                    ResponseCode = @"Operation completed successfully"
                };
            }
            return new ResponseDto(){
                IsSuccessful = false,
                ResponseCode = @"Something went wrong."
            };
        }


        [HttpDelete]
        public ResponseDto Delete(DriverDataDto driverActivityDto)
        {
            if (!ModelState.IsValid || driverActivityDto == null)
            {
                return new ResponseDto() {IsSuccessful = false, ResponseCode = "Invalid data"};
            }   

            _driverActivityService = new DriverActivityService();
            if (_driverActivityService.DeleteDriverItem(driverActivityDto.DriverId))
            {
                return new ResponseDto()
                {
                    IsSuccessful = true,
                    ResponseCode = @"Operation completed successfully"
                };
            }
            return new ResponseDto()
            {
                IsSuccessful = false,
                ResponseCode = @"Something went wrong."
            };
        }
    }
}