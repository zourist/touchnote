﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Xml.Linq;
using TechExercise.Models;

namespace TechExercise.Controllers
{
    public class LogisticsManagerRssFeedApiController : ApiController
    {
        string feedUrl = "http://www.logisticsmanager.com/feed/";
        int maxFeed = 4;
        
        public List<RssNewsDto> Get()
        {
            var webClient = new WebClient();

            string result = webClient.DownloadString(feedUrl);

            XDocument document = XDocument.Parse(result);

            return (from descendant in document.Descendants("item")
                    select new RssNewsDto()
                    {
                        Description = descendant.Element("description").Value,
                        Title = descendant.Element("title").Value,
                        Link = descendant.Element("link").Value,                        
                        PubDate = Convert.ToDateTime(descendant.Element("pubDate").Value)
                    }).Take(maxFeed).ToList();
        }
    }
}
